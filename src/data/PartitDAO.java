package data;

import models.Equip;
import models.EstadistiquesPartit;
import models.Lliga;
import models.Partit;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class PartitDAO {
    Lliga lliga = new Lliga();
    EquipDAO equipDAO = new EquipDAO();

    public void insert(Partit e){
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "INSERT INTO partits (Equip1id, Equip2id, Idguanyador, k1, d1, a1, cs1, ntorres1, ndracs1, nnash1, k2, d2, a2, cs2, ntorres2, ndracs2, nnash2) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, e.getEquip1Id().getId());
            insertStatement.setInt(2, e.getEquip2Id().getId());
            insertStatement.setInt(3, e.getGuanyador());
            insertStatement.setInt(4, e.getEquip1().getK());
            insertStatement.setInt(5, e.getEquip1().getD());
            insertStatement.setInt(6, e.getEquip1().getA());
            insertStatement.setInt(7, e.getEquip1().getCs());
            insertStatement.setInt(8, e.getEquip1().getNtorres());
            insertStatement.setInt(9, e.getEquip1().getNdracs());
            insertStatement.setInt(10, e.getEquip1().getNnashs());
            insertStatement.setInt(11, e.getEquip2().getK());
            insertStatement.setInt(12, e.getEquip2().getD());
            insertStatement.setInt(13, e.getEquip2().getA());
            insertStatement.setInt(14, e.getEquip2().getCs());
            insertStatement.setInt(15, e.getEquip2().getNtorres());
            insertStatement.setInt(16, e.getEquip2().getNdracs());
            insertStatement.setInt(17, e.getEquip2().getNnashs());
            insertStatement.execute();
            equipDAO.updatePGuanyats(lliga.getEquipById(e.getGuanyador(), equipDAO.list()));
        }catch (SQLException s){
            System.out.println(s.getMessage());
        }
    }

    public List<Partit> list(){
        List<Partit> list = new ArrayList<>();
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "SELECT * FROM partits";
            Statement listStatement = connection.createStatement();
            ResultSet resultat = listStatement.executeQuery(query);

            while (resultat.next()){
                int id = resultat.getInt("id");
                int equip1id = resultat.getInt("equip1id");
                int equip2id = resultat.getInt("equip2id");
                int idguanyador = resultat.getInt("idguanyador");
                int k1 = resultat.getInt("k1");
                int d1 = resultat.getInt("d1");
                int a1 = resultat.getInt("a1");
                int cs1 = resultat.getInt("cs1");
                int ntorres1 = resultat.getInt("ntorres1");
                int ndracs1 = resultat.getInt("ndracs1");
                int nnash1  = resultat.getInt("nnash1");
                int k2 = resultat.getInt("k2");
                int d2 = resultat.getInt("d2");
                int a2 = resultat.getInt("a2");
                int cs2 = resultat.getInt("cs2");
                int ntorres2 = resultat.getInt("ntorres2");
                int ndracs2 = resultat.getInt("ndracs2");
                int nnash2 = resultat.getInt("nnash2");

                Equip eq1 = lliga.getEquipById(equip1id, equipDAO.list());
                Equip eq2 = lliga.getEquipById(equip2id, equipDAO.list());

                list.add(new Partit(id, eq1, eq2, idguanyador, new EstadistiquesPartit(k1, d1, a1, cs1, ntorres1, ndracs1, nnash1), new EstadistiquesPartit(k2, d2, a2, cs2, ntorres2, ndracs2, nnash2)));
            }
        }catch (SQLException s){
            System.out.println(s.getMessage());
        }
        return list;
    }

    public boolean delete(int id){
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "DELETE FROM partits WHERE id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, id);
            insertStatement.execute();
            return true;
        }catch (SQLException s){
            System.out.println(s.getMessage());
        }
        return false;
    }


    public boolean update(Partit e){
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "UPDATE partits SET (Equip1id, Equip2id, Idguanyador, k1, d1, a1, cs1, ntorres1, ndracs1, nnash1, k2, d2, a2, cs2, ntorres2, ndracs2, nnash2) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) WHERE id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, e.getEquip1Id().getId());
            insertStatement.setInt(2, e.getEquip2Id().getId());
            insertStatement.setInt(3, e.getGuanyador());
            insertStatement.setInt(4, e.getEquip1().getK());
            insertStatement.setInt(5, e.getEquip1().getD());
            insertStatement.setInt(6, e.getEquip1().getA());
            insertStatement.setInt(7, e.getEquip1().getCs());
            insertStatement.setInt(8, e.getEquip1().getNtorres());
            insertStatement.setInt(9, e.getEquip1().getNdracs());
            insertStatement.setInt(10, e.getEquip1().getNnashs());
            insertStatement.setInt(11, e.getEquip2().getK());
            insertStatement.setInt(12, e.getEquip2().getD());
            insertStatement.setInt(13, e.getEquip2().getA());
            insertStatement.setInt(14, e.getEquip2().getCs());
            insertStatement.setInt(15, e.getEquip2().getNtorres());
            insertStatement.setInt(16, e.getEquip2().getNdracs());
            insertStatement.setInt(17, e.getEquip2().getNnashs());
            insertStatement.setInt(18, e.getId());

            insertStatement.execute();
            return true;
        }catch (SQLException s){
            System.out.println(s.getMessage());
        }
        return false;
    }
}
