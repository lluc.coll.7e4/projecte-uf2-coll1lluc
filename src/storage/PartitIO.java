package storage;

import data.EquipDAO;
import data.PartitDAO;
import models.EstadistiquesPartit;
import models.Lliga;
import models.Partit;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PartitIO {
    private Path PartitsPath;
    private Lliga lliga;
    private EquipDAO equipDAO;
    private PartitDAO partitDAO;

        public PartitIO(String PartitsPath){
            this.PartitsPath = Path.of(PartitsPath);
            lliga = new Lliga();
            equipDAO = new EquipDAO();
            partitDAO = new PartitDAO();
        }

        public void read(){
            try {
                if(Files.exists(PartitsPath)){
                    List<Partit> partits = readPartits();
                    partits.forEach(partitDAO::insert);
                }
            } catch (IOException e){
                System.err.println("Error llegint els partits.");
            }
        }

        public void write(){
            try {
                List<Partit> partits = partitDAO.list();
                writePartits(partits);
            }catch (IOException e){
                System.err.println("Error guardant els partits.");
            }
        }

        private void writePartits(List<Partit> partits) throws IOException {
            OutputStream outputStream = Files.newOutputStream(PartitsPath, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
            PrintStream printStream = new PrintStream(outputStream, true);

            printStream.println(partits.size());
            for(Partit p: partits){
                printStream.printf("%d %d %d %d\n %d %d %d %d %d %d %d\n %d %d %d %d %d %d %d\n",p.getId(), p.getEquip1Id().getId(), p.getEquip2Id().getId(), p.getGuanyador(),
                        p.getEquip1().getK(), p.getEquip1().getD(), p.getEquip1().getA(), p.getEquip1().getCs(), p.getEquip1().getNtorres(), p.getEquip1().getNdracs(), p.getEquip1().getNnashs(),
                        p.getEquip2().getK(), p.getEquip2().getD(), p.getEquip2().getA(), p.getEquip2().getCs(), p.getEquip2().getNtorres(), p.getEquip2().getNdracs(), p.getEquip2().getNnashs());
            }
        }

        private List<Partit> readPartits() throws IOException {
            Scanner scannerFitxer = new Scanner(PartitsPath);

            List<Partit> partits = new ArrayList<Partit>();
            int mida = scannerFitxer.nextInt();

            for(int i = 0; i < mida; i++){
                int id = scannerFitxer.nextInt();
                int equip1id = scannerFitxer.nextInt();
                int equip2id = scannerFitxer.nextInt();
                int guanyador = scannerFitxer.nextInt();
                partits.add(new Partit(id, lliga.getEquipById(equip1id, equipDAO.list()), lliga.getEquipById(equip2id, equipDAO.list()), guanyador, estadistiques(scannerFitxer), estadistiques(scannerFitxer)));
            }

            return partits;
        }
        private EstadistiquesPartit estadistiques(Scanner sc){
            int k = sc.nextInt();
            int d = sc.nextInt();
            int a = sc.nextInt();
            int cs = sc.nextInt();
            int ntorres = sc.nextInt();
            int ndracs = sc.nextInt();
            int nnashs = sc.nextInt();
            return new EstadistiquesPartit(k, d, a, cs, ntorres, ndracs, nnashs);
        }
}