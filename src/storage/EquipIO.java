package storage;

import data.EquipDAO;
import models.Equip;
import models.Lliga;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class EquipIO {
    private Path equipsPath;
    private EquipDAO equipDAO;

    public EquipIO(String equipsPath){
        this.equipsPath = Path.of(equipsPath);
        equipDAO = new EquipDAO();
    }

    public void read(){
        try {
            if(Files.exists(equipsPath)){
                List<Equip> equips = readEquips();
                equips.forEach(equipDAO::insert);
            }
        } catch (IOException e){
            System.err.println("Error llegint els equips.");
        }

    }

    public void write(){
        try {
            List<Equip> equips = equipDAO.list();
            writeEquips(equips);
        }catch (IOException e){
            System.err.println("Error guardant els equips.");
        }
    }

    private void writeEquips(List<Equip> equips) throws IOException {
        OutputStream outputStream = Files.newOutputStream(equipsPath, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        PrintStream printStream = new PrintStream(outputStream, true);

        printStream.println(equips.size());
        for(Equip e : equips){
            printStream.printf("%d %s\n%s\n", e.getId(), e.getNom(), e.getAbreviacio());
        }
    }

    private List<Equip> readEquips() throws IOException {
        Scanner scannerFitxer = new Scanner(equipsPath);

        List<Equip> equipss = new ArrayList<Equip>();
        int mida = scannerFitxer.nextInt();

        for(int i = 0; i < mida; i++){
            int id = scannerFitxer.nextInt();
            String nom = scannerFitxer.nextLine().replace(" ", "");;
            String abreviacio = scannerFitxer.next().replace(" ", "");
            equipss.add(new Equip(id, nom, abreviacio, 0));
        }

        return equipss;
    }
}
