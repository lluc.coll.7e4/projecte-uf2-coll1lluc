package models;

public class Partit {
    private int id;
    private Equip equip1Id;
    private Equip equip2Id;
    private int guanyador;
    private EstadistiquesPartit equip1;
    private EstadistiquesPartit equip2;

    public Partit(int id, Equip equip1Id, Equip equip2Id, int guanyador, EstadistiquesPartit equip1, EstadistiquesPartit equip2){
        this.id = id;
        this.equip1Id = equip1Id;
        this.equip2Id = equip2Id;
        this.guanyador = guanyador;
        this.equip1 = equip1;
        this.equip2 = equip2;
    }

    public int getId() {
        return id;
    }

    public Equip getEquip1Id() {
        return equip1Id;
    }

    public Equip getEquip2Id() {
        return equip2Id;
    }

    public int getGuanyador() {
        return guanyador;
    }

    public EstadistiquesPartit getEquip1() {
        return equip1;
    }

    public EstadistiquesPartit getEquip2() {
        return equip2;
    }
}
