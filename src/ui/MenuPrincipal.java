package ui;

import data.Database;
import data.EquipDAO;
import data.PartitDAO;
import models.Equip;
import models.Lliga;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class MenuPrincipal {
    Scanner sc;
    MenuDades menuDades;
    MenuEstadistiques menuEstadistiques;
    Lliga lliga;
    EquipDAO equipDAO;
    PartitDAO partitDAO;

    public MenuPrincipal(){
        this.sc = new Scanner(System.in).useLocale(Locale.US);
        menuDades = new MenuDades(sc);
        menuEstadistiques = new MenuEstadistiques(sc);
        equipDAO = new EquipDAO();
        partitDAO = new PartitDAO();
        lliga = new Lliga();
    }

    /**
     * Demana una accio valida i la retorna
     * @param nombreMaxim
     * @param sc
     * @return
     */
    public static int nextIntMax(int nombreMaxim, Scanner sc){
        int accio = sc.nextInt();
        while (accio < 0 || accio > nombreMaxim) {
            System.out.println("ERROR! L'opció introduida està fora del rang.");
            accio = sc.nextInt();
        }
        return accio;
    }

    /**
     * Mostra el menu principal
     */
    public void mostrarMenu(){
        while(true){
            System.out.println("Benvinguts al gestor de estadistiques de la LEC");
            System.out.println("0. Sortir");
            System.out.println("1. Menu dades");
            System.out.println("2. Menu estadistiques");
            System.out.println("3. Classificacio");
            System.err.println("4. Reiniciar taules (combinar amb el 'Importar' del menu dades per tal de recuperar les dades)");
            System.err.println("Opcio no recomenada");

            int acc = nextIntMax(4, sc);
            switch (acc){
                case 0:
                    return;
                case 1:
                    menuDades.MostrarMenuDades();
                    System.out.println();
                    break;
                case 2:
                    menuEstadistiques.MostrarMenuEstadistiques();
                    System.out.println();
                    break;
                case 3:
                    lliga.Classificacio(equipDAO.list());
                    System.out.println();
                    break;
                case 4:
                    equipDAO.deleteAndCreate();
            }
        }
    }
}