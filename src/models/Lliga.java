package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Lliga {

    /**
     * Imprimeix la llista dels equips
     */
    public void llistaEquips(List<Equip> equips){
        for(Equip e: equips){
            System.out.printf("%d. %s - (%s)\n", e.getId(), e.getNom(), e.getAbreviacio());
        }
    }

    /**
     * Mira quin equip es mitjançat la id d'aquest
     * @param id
     * @return
     */
    public Equip getEquipById(int id, List<Equip> equips){
        for(Equip e : equips){
            if(e.getId() == id)
                return e;
        }
        return null;
    }

    /**
     * Printa la llista partits
     */
    public void llistaPartits(List<Partit> partits){
        for (Partit p: partits){
            if (p.getEquip1Id().getId() == p.getGuanyador()){
                System.out.printf("%d.  %sWIN (%s) - %d/%d/%d ---- %d/%d/%d - (%s) %s\n", p.getId(), p.getEquip1Id().getNom(), p.getEquip1Id().getAbreviacio(), p.getEquip1().getK(), p.getEquip1().getD(),
                        p.getEquip1().getA(), p.getEquip2().getK(), p.getEquip2().getD(), p.getEquip2().getA(), p.getEquip2Id().getAbreviacio(), p.getEquip2Id().getNom());
            }
            else if (p.getEquip2Id().getId() == p.getGuanyador()){
                System.out.printf("%d.  %s (%s) - %d/%d/%d ---- %d/%d/%d - (%s) %sWIN\n", p.getId(), p.getEquip1Id().getNom(), p.getEquip1Id().getAbreviacio(), p.getEquip1().getK(), p.getEquip1().getD(),
                        p.getEquip1().getA(), p.getEquip2().getK(), p.getEquip2().getD(), p.getEquip2().getA(), p.getEquip2Id().getAbreviacio(), p.getEquip2Id().getNom());
            }
            else{
                System.out.printf("%d.  %s (%s) - %d/%d/%d ---- %d/%d/%d - (%s) %s\n", p.getId(), p.getEquip1Id().getNom(), p.getEquip1Id().getAbreviacio(), p.getEquip1().getK(), p.getEquip1().getD(),
                        p.getEquip1().getA(), p.getEquip2().getK(), p.getEquip2().getD(), p.getEquip2().getA(), p.getEquip2Id().getAbreviacio(), p.getEquip2Id().getNom());
            }
        }
    }

    /**
     * Mira quin equi es mitjançant la id
     * @param id
     * @return
     */
    public Partit getPartitById(int id, List<Partit> partits){
        for(Partit p: partits){
            if(p.getId() == id)
                return p;
        }
        return null;
    }

    /**
     * Calcula el KDA de tots els partits
     */
    public void kdaMitjana(List<Partit> partits){
        double kdatotal = 0;
        for (int i=0; i<partits.size(); i++){
            double kda1 = (partits.get(i).getEquip1().getK() + partits.get(i).getEquip1().getA() ) / partits.get(i).getEquip1().getD();
            double kda2 = (partits.get(i).getEquip2().getK() + partits.get(i).getEquip2().getA() ) / partits.get(i).getEquip2().getD();
            kdatotal += kda1 +kda2;
        }
        double mitjana = kdatotal / (partits.size() * 2);
        System.out.printf("%.2f\n", mitjana);
    }

    /**
     * Calcula quin ha estat el partit amb el millor KDA combinat
     */
    public void Partitmillorkda(List<Partit> partits){
        double kdatotal = 0;
        double kdatop = 0;
        Partit partit = partits.get(0);
        for (int i=0; i<partits.size(); i++) {
            double kda1 = (partits.get(i).getEquip1().getK() + partits.get(i).getEquip1().getA()) / partits.get(i).getEquip1().getD();
            double kda2 = (partits.get(i).getEquip2().getK() + partits.get(i).getEquip2().getA()) / partits.get(i).getEquip2().getD();
            kdatotal += (kda1 + kda2) / 2;
            if (kdatotal > kdatop){
                partit = partits.get(i);
                kdatop = kdatotal;
            }
        }
        System.out.printf("%s --- vs --- %s ----- kda = %.2f\n", partit.getEquip1Id().getNom(), partit.getEquip2Id().getNom(), kdatop);
    }

    /**
     * Calcula el nombre total d'objectius totals
     */
    public void Nombretotalobjectius(List<Partit> partits){
        int totaldracs = 0;
        int totalnash = 0;
        int totaltorres = 0;
        for (int i=0; i<partits.size(); i++){
            totaldracs += partits.get(i).getEquip1().getNdracs();
            totalnash += partits.get(i).getEquip1().getNnashs();
            totaltorres += partits.get(i).getEquip1().getNtorres();
            totaldracs += partits.get(i).getEquip2().getNdracs();
            totalnash += partits.get(i).getEquip2().getNnashs();
            totaltorres += partits.get(i).getEquip2().getNtorres();
        }
        System.out.printf("Nombre de dracs: %d\n", totaldracs);
        System.out.printf("Nombre de nashors: %d\n", totalnash);
        System.out.printf("Nombre de torres: %d\n", totaltorres);
    }

    /**
     * Imprimeix el nombre d'equips i el nombre de partits
     */
    public void ntotalllistes(List<Equip> equips, List<Partit> partits){
        System.out.println("El nombre d'equips total es: "+equips.size());
        System.out.println("El nombre total de partits a la temporada Spring 2021: "+partits.size());
    }

    /**
     * Imprimeix les estadistiques per partit
     * @param p
     */
    public void PerPertit(Partit p){
        if (p.getEquip1Id().getId() == p.getGuanyador()){
            System.out.println("                  GUANYADOR = "+p.getEquip1Id().getNom()+" ("+p.getEquip1Id().getAbreviacio()+")");
        }
        else if (p.getEquip2Id().getId() == p.getGuanyador()){
            System.out.println("                  GUANYADOR = "+p.getEquip2Id().getNom()+" ("+p.getEquip2Id().getAbreviacio()+")");
        }
        System.out.printf("                      (%s) -- vs -- (%s) \n", p.getEquip1Id().getAbreviacio(), p.getEquip2Id().getAbreviacio());
        System.out.printf("KDA ->              %d/%d/%d ------- %d/%d/%d\n", p.getEquip1().getK(), p.getEquip1().getD(), p.getEquip1().getA(), p.getEquip2().getK(), p.getEquip2().getD(), p.getEquip2().getA());
        System.out.printf("CS ->                %d -------------- %d\n", p.getEquip1().getCs(), p.getEquip2().getCs());
        System.out.printf("Torres tirades ->     %d ---------------- %d\n", p.getEquip1().getNtorres(), p.getEquip2().getNtorres());
        System.out.printf("Dracs ->              %d ---------------- %d\n", p.getEquip1().getNdracs(), p.getEquip2().getNdracs());
        System.out.printf("Nashors ->            %d ---------------- %d\n", p.getEquip1().getNnashs(), p.getEquip2().getNnashs());
    }

    /**
     * Calcula i imprimeix les estadistiques totals per equip
     * @param e
     */
    public void PerEquip(Equip e, List<Partit> partits){
        int nk = 0;
        int nd = 0;
        int na = 0;
        int pguanyats = 0;
        int pperduts = 0;
        int ndracs = 0;
        int nnash = 0;
        int ntorres = 0;
        for (int i = 0; i < partits.size(); i++) {
            if(e.getId() == partits.get(i).getEquip1Id().getId() || e.getId() == partits.get(i).getEquip2Id().getId()) {
                if (e.getId() == partits.get(i).getGuanyador()) {
                    pguanyats += 1;
                } else {
                    pperduts += 1;
                }
            }
            if (e.getId() == partits.get(i).getEquip1Id().getId()){
                nk += partits.get(i).getEquip1().getK();
                nd += partits.get(i).getEquip1().getD();
                na += partits.get(i).getEquip1().getA();
                ndracs += partits.get(i).getEquip1().getNdracs();
                nnash += partits.get(i).getEquip1().getNnashs();
                ntorres += partits.get(i).getEquip1().getNtorres();
            }
            else if (e.getId() == partits.get(i).getEquip2Id().getId()){
                nk += partits.get(i).getEquip2().getK();
                nd += partits.get(i).getEquip2().getD();
                na += partits.get(i).getEquip2().getA();
                ndracs += partits.get(i).getEquip2().getNdracs();
                nnash += partits.get(i).getEquip2().getNnashs();
                ntorres += partits.get(i).getEquip2().getNtorres();
            }
        }
        double kda = (nk + na) /  nd;
        System.out.printf("%s - (%s):\n", e.getNom(), e.getAbreviacio());
        System.out.printf("KDA mitja = %.2f --- %d/%d/%d\n", kda, nk, nd, na);
        System.out.printf("Partits guanyats -> %d\n", pguanyats);
        System.out.printf("Partits perduts -> %d\n", pperduts);
        System.out.printf("Nombre de dracs: %d\n", ndracs);
        System.out.printf("Nombre de nashors: %d\n", nnash);
        System.out.printf("Nombre de torres: %d\n", ntorres);
    }

    /**
     * Calcula la classificacio de la lliga
     */
    public void Classificacio(List<Equip> equips){
        Collections.sort(equips, (r1, r2) -> Integer.compare(r2.getPguanyats(), r1.getPguanyats()));
        int cont = 0;
        int posicio = 1;
        System.out.println("Classficacio LEC Spring 2021:");
        while (cont < equips.size()){
            System.out.printf("%d. %s - (%s) --- %d WINS\n", posicio, equips.get(cont).getNom(), equips.get(cont).getAbreviacio(), equips.get(cont).getPguanyats());
            if (cont < equips.size()-1) {
                if (equips.get(cont).getPguanyats() != equips.get(cont+1).getPguanyats()) {
                    posicio++;
                }
            }
            cont++;
        }
    }
}
