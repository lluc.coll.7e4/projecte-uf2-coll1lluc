package models;

public class EstadistiquesPartit {
    private int k;
    private int d;
    private int a;
    private int cs;
    private int ntorres;
    private int ndracs;
    private int nnashs;

    public EstadistiquesPartit (int k, int d, int a, int cs, int ntorres, int ndracs, int nnashs){
        this.k = k;
        this.d = d;
        this.a = a;
        this.cs = cs;
        this.ntorres = ntorres;
        this.ndracs = ndracs;
        this.nnashs = nnashs;
    }

    public int getK() {
        return k;
    }

    public int getD() {
        return d;
    }

    public int getA() {
        return a;
    }

    public int getCs() {
        return cs;
    }

    public int getNtorres() {
        return ntorres;
    }

    public int getNdracs() {
        return ndracs;
    }

    public int getNnashs() {
        return nnashs;
    }
}
