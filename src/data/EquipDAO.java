package data;

import models.Equip;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EquipDAO {
    public void insert(Equip e){
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "INSERT INTO equips (nom, abreviacio, pguanyats) VALUES(?, ?, ?)";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1, e.getNom());
            insertStatement.setString(2, e.getAbreviacio());
            insertStatement.setInt(3, e.getPguanyats());
            insertStatement.execute();
        }catch (SQLException s){
            System.out.println(s.getMessage());
        }
    }

    public List<Equip> list(){
        List<Equip> list = new ArrayList<>();
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "SELECT * FROM equips ORDER BY id";
            Statement listStatement = connection.createStatement();
            ResultSet resultat = listStatement.executeQuery(query);

            while (resultat.next()){
                int id = resultat.getInt("id");
                String nom = resultat.getString("nom");
                String abreviacio = resultat.getString("Abreviacio");
                int pguanyats = resultat.getInt("Pguanyats");
                list.add(new Equip(id, nom, abreviacio, pguanyats));
            }
        }catch (SQLException s){
            System.out.println(s.getMessage());
        }
        return list;
    }

    public boolean delete(int id){
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "DELETE FROM equips WHERE id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, id);
            insertStatement.execute();
            return true;
        }catch (SQLException s){
            System.out.println(s.getMessage());
        }
        return false;
    }

    public boolean updatePGuanyats(Equip e){
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "UPDATE equips SET(nom, abreviacio, pguanyats) = (?, ?, ?) WHERE id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1, e.getNom());
            insertStatement.setString(2, e.getAbreviacio());
            insertStatement.setInt(3, e.getPguanyats()+1);
            insertStatement.setInt(4, e.getId());
            insertStatement.execute();
            return true;
        }catch (SQLException s){
            System.out.println(s.getMessage());
        }
        return false;
    }

    public boolean update(Equip e){
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "UPDATE equips SET(nom, abreviacio, pguanyats) = (?, ?, ?) WHERE id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1, e.getNom());
            insertStatement.setString(2, e.getAbreviacio());
            insertStatement.setInt(3, e.getPguanyats());
            insertStatement.setInt(4, e.getId());
            insertStatement.execute();
            return true;
        }catch (SQLException s){
            System.out.println(s.getMessage());
        }
        return false;
    }

    public void deleteAndCreate() {
        try {
            Connection connection = Database.getInstance().getConnection();
            String borrarPartits = "DROP TABLE partits";
            String borrarEquips = "DROP TABLE equips";
            PreparedStatement BPartits = connection.prepareStatement(borrarPartits);
            PreparedStatement BEquips = connection.prepareStatement(borrarEquips);
            BPartits.execute();
            BEquips.execute();
            System.out.println("Taules borrades correctament");
        } catch (SQLException e) {
            System.err.println("Error borrant");
        }
        try {
            Connection connection = Database.getInstance().getConnection();
            String crearEquips = "create table Equips (\n" +
                    "   id serial,\n" +
                    "   nom varchar,\n" +
                    "   abreviacio varchar,\n" +
                    "   pguanyats int,\n" +
                    " \n" +
                    "   constraint pk_id primary key (id))";
            String crearPartits = "create table Partits (\n" +
                    "   id serial,\n" +
                    "   Equip1id int,\n" +
                    "   Equip2id int,\n" +
                    "   idGuanyador int,\n" +
                    "   k1 int,\n" +
                    "   d1 int,\n" +
                    "   a1 int,\n" +
                    "   cs1 int,\n" +
                    "   ntorres1 int,\n" +
                    "   ndracs1 int,\n" +
                    "   nnash1 int,\n" +
                    "   k2 int,\n" +
                    "   d2 int,\n" +
                    "   a2 int,\n" +
                    "   cs2 int,\n" +
                    "   ntorres2 int,\n" +
                    "   ndracs2 int,\n" +
                    "   nnash2 int,\n" +
                    "   \n" +
                    "   constraint pk_idp primary key (id),\n" +
                    "   constraint fk_Equip1id foreign key (Equip1id) references Equips (id) ON DELETE CASCADE,\n" +
                    "    constraint fk_Equip2id foreign key (Equip2id) references Equips (id) ON DELETE CASCADE);\n";
            PreparedStatement CEquips = connection.prepareStatement(crearEquips);
            PreparedStatement CPartits = connection.prepareStatement(crearPartits);
            CEquips.execute();
            CPartits.execute();
            System.out.println("Taules creades correctament");
        } catch (SQLException e) {
            System.err.println("Error creant");
        }
    }
}
