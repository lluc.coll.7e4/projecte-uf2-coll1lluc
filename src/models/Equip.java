package models;

public class Equip {
    private int id;
    private String nom;
    private String abreviacio;
    private int pguanyats;


    public Equip (int id, String nom, String abreviacio, int pguanyats){
        this.id = id;
        this.nom = nom;
        this.abreviacio = abreviacio;
        this.pguanyats = pguanyats;
    }

    public int getId() {
        return id;
    }
    public String getNom() {
        return nom;
    }
    public String getAbreviacio() {
        return abreviacio;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setAbreviacio(String abreviacio) {
        this.abreviacio = abreviacio;
    }
    public int getPguanyats() {
        return pguanyats;
    }

    public void setPguanyats(int pguanyats) {
        this.pguanyats = pguanyats;
    }
}
