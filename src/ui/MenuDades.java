package ui;

import data.EquipDAO;
import data.PartitDAO;
import models.Equip;
import models.EstadistiquesPartit;
import models.Lliga;
import models.Partit;
import storage.EquipIO;
import storage.PartitIO;

import java.util.Scanner;

public class MenuDades {
    Scanner sc;
    Lliga lliga = new Lliga();
    EquipDAO equipDAO;
    PartitDAO partitDAO;
    EquipIO equipIO;
    PartitIO partitIO;

    public MenuDades(Scanner sc){
        this.sc = sc;
        equipIO = new EquipIO("files/Equips.txt");
        partitIO = new PartitIO("files/Partits.txt");
        equipDAO = new EquipDAO();
        partitDAO = new PartitDAO();
    }

    /**
     * Mostra el menu dades
     */
    public void MostrarMenuDades(){
        while(true){
            System.out.println("Benvingut al menu dades:");
            System.out.println("1. Afegir equip");
            System.out.println("2. Actualitzar equip");
            System.out.println("3. Eliminar equip");
            System.out.println("4. Llista equips");
            System.out.println("5. Afegir partit");
            System.out.println("6. Actualitzar equip");
            System.out.println("7. Eliminar equip");
            System.out.println("8. Llista partits");
            System.out.println("9. Import to database");
            System.out.println("10. Export from database");

            System.out.println("0. Return");

            int acc = MenuPrincipal.nextIntMax(10, sc);
            switch (acc){
                case 0:
                    return;
                case 1:
                    System.out.println("Introduir Equip:");
                    introduirEquip();
                    System.out.println();
                    break;
                case 2:
                    actualitzarEquip();
                    System.out.println();
                    break;
                case 3:
                    System.out.println("Recorda que eliminant un equip, eliminaras tots els partits als quals participi");
                    System.out.println("Estas segur que vols continuar? (S/N)");
                    String s = sc.nextLine();
                    if (s.toUpperCase().equals("N")){
                        break;
                    }
                    System.out.println();
                    deleteEquip();
                    System.out.println();
                    break;
                case 4:
                    llistaEquips();
                    System.out.println();
                    break;
                case 5:
                    addPartit();
                    System.out.println();
                    break;
                case 6:
                    actualitzarPartit();
                    System.out.println();
                    break;
                case 7:
                    String r = sc.nextLine();
                    if (r.toUpperCase().equals("N")){
                        break;
                    }
                    System.out.println();
                    deletePartit();
                    System.out.println();
                    break;
                case 8:
                    llistaPartits();
                    System.out.println();
                    break;
                case 9:
                    equipIO.read();
                    partitIO.read();
                    System.out.println("Dades importades correctament");
                    break;
                case 10:
                    equipIO.write();
                    partitIO.write();
                    System.out.println("Dades baixades correctament");
                    break;
            }
        }
    }

    /**
     * Clase amb les instruccions per afegir un equip a la llista
     */
    private void introduirEquip(){
        System.out.println();
        System.out.println("Introdueix el nom del equip:");
        sc.nextLine();
        String nom = sc.nextLine();
        System.out.println("Introdueix la abreviacio del equip:");
        String abreviacio = sc.next();
        equipDAO.insert(new Equip(0, nom, abreviacio, 0));
        System.out.println("L'equip"+nom+" ("+abreviacio+") s'ha afegit cxorrectament");
    }

    private void actualitzarEquip(){
        llistaEquips();
        System.out.println("Introdueixi el id del equip que vol actualitzar");
        int id = sc.nextInt();
        sc.nextLine();
        System.out.println("Introdueix el nom:");
        String nom = sc.nextLine();
        System.out.println("Introdueix la abreviacio del equip:");
        String abreviacio = sc.next();
        equipDAO.update(new Equip(id, nom, abreviacio, lliga.getEquipById(id, equipDAO.list()).getPguanyats()));
        System.out.println("L'equip"+nom+" ("+abreviacio+") s'ha actualitzat cxorrectament");
    }

    private void deleteEquip(){
        llistaEquips();
        System.out.println("Introdueixi el id del equip que vol eliminar");
        int id = sc.nextInt();
        equipDAO.delete(id);
        System.out.println("Equip eliminat correctament");
    }
    /**
     * Imprimir llista equips
     */
    private void llistaEquips(){
        System.out.println("Llista equips: ");
        lliga.llistaEquips(equipDAO.list());
    }

    /**
     * Clase amb les instruccions per afegir un partit a la llista
     */
    private void addPartit(){
        System.out.println("Introduir Partit");
        llistaEquips();
        System.out.println("Introdueix ID del primer equip:");
        int equip1id = sc.nextInt();
        System.out.println("Introdueix ID del segon equip:");
        int equip2id = sc.nextInt();
        System.out.println("Introdueix l'ID del guanyador del partit");
        int guanyador = sc.nextInt();
        System.out.println("I ara les estadistiques del primer equip:");
        EstadistiquesPartit equip1 = estadistiques();
        System.out.println("I ara les estadistiques del segon equip:");
        EstadistiquesPartit equip2 = estadistiques();
        Equip eq1 = lliga.getEquipById(equip1id, equipDAO.list());
        Equip eq2 = lliga.getEquipById(equip2id, equipDAO.list());
        partitDAO.insert(new Partit(0, eq1, eq2, guanyador, equip1, equip2));
        System.out.printf("Partit entre %s i %s s'ha afegit correctament", eq1.getNom(), eq2.getNom());
    }

    /**
     * Classe per afegir les estadistiques a la anterior classe
     * @return
     */
    private EstadistiquesPartit estadistiques(){
        System.out.println("Kills, Morts, Assistencies i CS totals del equip:");
        int k = sc.nextInt();
        int d = sc.nextInt();
        int a = sc.nextInt();
        int cs = sc.nextInt();
        System.out.println("Numero de torres tirades, dracs i de nashors del equip:");
        int ntorres = sc.nextInt();
        int ndracs = sc.nextInt();
        int nnashs = sc.nextInt();
        return new EstadistiquesPartit(k, d, a, cs, ntorres, ndracs, nnashs);
    }

    private void actualitzarPartit(){
        llistaPartits();
        System.out.println("Introdueixi la id del partit que vol actualitzar");
        int id = sc.nextInt();
        System.out.println("Introdueix ID del primer equip:");
        int equip1id = sc.nextInt();
        System.out.println("Introdueix ID del segon equip:");
        int equip2id = sc.nextInt();
        System.out.println("Introdueix l'ID del guanyador del partit");
        int guanyador = sc.nextInt();
        System.out.println("I ara les estadistiques del primer equip:");
        EstadistiquesPartit equip1 = estadistiques();
        System.out.println("I ara les estadistiques del segon equip:");
        EstadistiquesPartit equip2 = estadistiques();
        Equip eq1 = lliga.getEquipById(equip1id, equipDAO.list());
        Equip eq2 = lliga.getEquipById(equip2id, equipDAO.list());
        partitDAO.update(new Partit(0, eq1, eq2, guanyador, equip1, equip2));
        equipDAO.updatePGuanyats(lliga.getEquipById(guanyador, equipDAO.list()));
        System.out.printf("Partit entre %s i %s s'ha actualitzat correctament", eq1.getNom(), eq2.getNom());
    }

    private void deletePartit(){
        llistaPartits();
        System.out.println("Introdueixi el id del partit que vol eliminar");
        int id = sc.nextInt();
        partitDAO.delete(id);
        System.out.println("Partit eliminat correctament");
    }

    /**
     * Imprimir partits
     */
    private void llistaPartits(){
        System.out.println("Llista partits: ");
        lliga.llistaPartits(partitDAO.list());
    }
}
