package ui;

import data.EquipDAO;
import data.PartitDAO;
import models.Equip;
import models.Lliga;
import models.Partit;

import java.util.Scanner;

public class MenuEstadistiques {
    Scanner sc;
    Lliga lliga = new Lliga();
    EquipDAO equipDAO;
    PartitDAO partitDAO;

    public MenuEstadistiques(Scanner sc){
        this.sc = sc;
        equipDAO = new EquipDAO();
        partitDAO = new PartitDAO();
    }

    /**
     * Mostrar menu estaditiques
     */
    public void MostrarMenuEstadistiques(){
        while(true){
            System.out.println("Benvingut al menu estadistiques:");
            System.out.println("1. Estadistiques globals");
            System.out.println("2. Estadistiques per equip");
            System.out.println("3. Estadistiques per partit");
            System.out.println("0. Return");

            int acc = MenuPrincipal.nextIntMax(3, sc);
            switch (acc) {
                case 0:
                    return;
                case 1:
                    System.out.println("Estadistiques globals:");
                    EstadistiquesGlobals();
                    System.out.println();
                    break;
                case 2:
                    System.out.println("Estadistiques per equip:");
                    EstadistiquesPerEquip();
                    System.out.println();
                    break;
                case 3:
                    System.out.println("Estadistiques per partit:");
                    EstadistiquesPerPartit();
                    System.out.println();
                    break;
            }
        }
    }

    /**
     * Mostra les estadistiques globals
     */
    private void EstadistiquesGlobals(){
        System.out.print("El kda de mitjana a la LEC és: ");
        lliga.kdaMitjana(partitDAO.list());
        System.out.print("El partit amb millor kda és: ");
        lliga.Partitmillorkda(partitDAO.list());
        System.out.println("El nombre total d'objectius a la LEC són: ");
        lliga.Nombretotalobjectius(partitDAO.list());
        lliga.ntotalllistes(equipDAO.list(), partitDAO.list());
    }

    /**
     * Dona les instruccions per mirar un partit amb profunditat i crida a la classe de "lliga" perque ho faci
     */
    private void EstadistiquesPerPartit(){
        System.out.println("Quin numero de partit es el que vols veure amb profunditat?");
        lliga.llistaPartits(partitDAO.list());
        int id = sc.nextInt();
        Partit partit = lliga.getPartitById(id, partitDAO.list());
        lliga.PerPertit(partit);
    }

    /**
     * Dona les instruccions per mirar un equip amb profunditat i crida a la classe de "lliga" perque ho faci
     */
    private void EstadistiquesPerEquip(){
        System.out.println("Quin es l'equip de qual vols veure les estadistiques?");
        lliga.llistaEquips(equipDAO.list());
        int id = sc.nextInt();
        Equip equip = lliga.getEquipById(id, equipDAO.list());
        lliga.PerEquip(equip, partitDAO.list());
    }
}
