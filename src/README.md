# Lliga LEC
#### Lluc Coll Ubia
<ul>El meu projecte es basa en la lliga de League of Legends Europea; la LEC

Es gestionen els equips (composats per id, nom, abrebiacio i numero de partits guanyats) i els partits (composats per
Equip1, Equip2, guanyador, estadistiques equip1, estadistiques equip2)

Les estadistiques estan composades per kills, morts, assistencies, cs, numero de dracs matats per l'equip,
numero de nashors matats per l'equip i numero de torres tirades a l'equip rival.

El programa calcula estadistiques globals de la lliga, per equips i per partits.
A mes tambe calcula la classificacio mitjançant el numero de victories.
</ul>
    
##### Introduir equip
![](https://gitlab.com/lluc.coll.7e4/projecte-uf2-coll1lluc/-/raw/master/src/Images/addequip.PNG)

##### Llista Equips
![](https://gitlab.com/lluc.coll.7e4/projecte-uf2-coll1lluc/-/raw/master/src/Images/llistequips.PNG)

##### Introduir Partit
![](https://gitlab.com/lluc.coll.7e4/projecte-uf2-coll1lluc/-/raw/master/src/Images/addPartit.PNG)

##### Llista Partits
![](https://gitlab.com/lluc.coll.7e4/projecte-uf2-coll1lluc/-/raw/master/src/Images/llistPartits.PNG)

##### Estadistiques Globals
![](https://gitlab.com/lluc.coll.7e4/projecte-uf2-coll1lluc/-/raw/master/src/Images/globals.PNG)

##### Estadistiques per equip
![](https://gitlab.com/lluc.coll.7e4/projecte-uf2-coll1lluc/-/raw/master/src/Images/perequip.PNG)

##### Estadistiques per partit
![](https://gitlab.com/lluc.coll.7e4/projecte-uf2-coll1lluc/-/raw/master/src/Images/perpartit.PNG)

##### Classificacio de la lliga
![](https://gitlab.com/lluc.coll.7e4/projecte-uf2-coll1lluc/-/raw/master/src/Images/classificacio.PNG)

### Metode de guardat
<ul>
Per tal de guardar la informacio utilitzarem dos fitxers: Un pels equips i l'altre pels partits
El equips es guardaran de la seguent manera:</ul>
<ul>id del equip | nom del equip | abreviacio del equip</ul>
<ul>Per altra banda, els partits es guardaran de manera similar:</ul><ul> id equip1 | id equip2 | guanyador2
 | estadistiques equip1 | estadistiques equip2</ul>