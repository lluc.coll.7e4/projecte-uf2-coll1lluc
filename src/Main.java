import data.Database;
import models.Lliga;
import storage.EquipIO;
import storage.PartitIO;
import ui.MenuPrincipal;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Lliga lliga = new Lliga();
        Database.getInstance().connect();

        MenuPrincipal menuPrincipal = new MenuPrincipal();
        menuPrincipal.mostrarMenu();

        Database.getInstance().close();
    }
}
